//let serVivo = (function (){
//
//})()
function serVivoDef(props) {
	let that = {};
	let enamoradoDe = "Princesa, Ruda, ya saben";

	for(let key in props) {
		that[key] = props[key]
	}

	that.getNombre = function () {
		return `${props.nombre} ${props.apellido}`
	};

	that.getEnamoradoDe = function () {
		return enamoradoDe;
	};

	return that;

};
let serVivo = serVivoDef({ nombre: "Jaime", apellido: "Cervantes", edad: 31 });
console.log(serVivo);
console.log(serVivo.getEnamoradoDe());

let persona = (function (props) {
	let that = serVivoDef(props);

	that.comer = function () {
		return `${props.nombre} esta comiendo...`;
	};

	return that;

})({ nombre: "Rodrigo", apellido: "Arenas", edad: 24 });
console.log(persona)
console.log(persona.comer());